package appiumtests;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.touch.offset.PointOption;

public class LoginMsisdnTest extends BaseClass {
	
	String msisdn=null;

	@BeforeSuite
	public static void goToLoginWithMsisdn_Ind() throws InterruptedException, WebDriverException, IOException {
		ExtentTest testGoToLoginMsisdn = extent.createTest("Go To Login via MSISDN", "Login using MSIS path");

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// Assert if the language button available
		MobileElement el1 = (MobileElement) driver.findElementByXPath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.Button[1]");
		MobileElement el2 = (MobileElement) driver.findElementByXPath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.Button[2]");

		Assert.assertTrue(el1.isDisplayed() && el2.isDisplayed());
		testGoToLoginMsisdn.log(Status.INFO, "Languange button displayed");

		// Assert the text

		Assert.assertEquals(el1.getText(), "Saya berbahasa Indonesia");

		Assert.assertEquals(el2.getText(), "I speak English");

		testGoToLoginMsisdn.log(Status.INFO, "Languange button text is correct", MediaEntityBuilder
				.createScreenCaptureFromBase64String(driver.getScreenshotAs(OutputType.BASE64)).build());
		// Choose IN Languange
		el1.click();
		Dimension size = driver.manage().window().getSize();
		int x = size.width / 2;
		int y = (int) (size.height - 50);
		new TouchAction(driver).tap(PointOption.point(x, y)).perform();

		testGoToLoginMsisdn.log(Status.INFO, "Choose Login via MSISDN", MediaEntityBuilder
				.createScreenCaptureFromBase64String(driver.getScreenshotAs(OutputType.BASE64)).build());

		el1 = (MobileElement) driver.findElementByXPath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[1]");
		el1.click();

		testGoToLoginMsisdn.log(Status.PASS, "Success Go to Login Form MSISDN");
	}

	@Test(enabled = true)
	public void loginUsingPospaidMsisdn() throws InterruptedException, IOException {
		ExtentTest testLoginPostpaid = extent.createTest("Login Postpaid", "Login using postpaid number");

		MobileElement msisdnInput = (MobileElement) driver.findElementByXPath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[2]/android.widget.EditText");
		MobileElement loginButton = (MobileElement) driver.findElementByXPath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[3]/android.widget.Button");
		Assert.assertTrue(msisdnInput.isDisplayed() && loginButton.isDisplayed());
		testLoginPostpaid.log(Status.INFO, "MSISDN Input & Login Button Displayed", MediaEntityBuilder
				.createScreenCaptureFromBase64String(driver.getScreenshotAs(OutputType.BASE64)).build());
		msisdn = "6285921996200";
		msisdnInput.setValue(msisdn);
		testLoginPostpaid.log(Status.INFO, "Trying Login using : "+msisdn, MediaEntityBuilder
				.createScreenCaptureFromBase64String(driver.getScreenshotAs(OutputType.BASE64)).build());
		loginButton.click();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

		MobileElement popupText = (MobileElement) driver.findElementByXPath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[2]/android.view.View[2]");
		Assert.assertTrue(popupText.isDisplayed());
		Assert.assertEquals(popupText.getText(),
				"Pelanggan terhormat, Anda akan membuka myXL prabayar. Namun, nomor Anda terdaftar sebagai pelanggan pascabayar Prioritas.");
		String popupMsg = driver.getScreenshotAs(OutputType.BASE64);
		driver.findElementByXPath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[2]/android.widget.Button[2]")
				.click();
		testLoginPostpaid.log(Status.PASS, "Login Postpaid passed",
				MediaEntityBuilder.createScreenCaptureFromBase64String(popupMsg).build());

	}

	@Test(enabled = true)
	public void loginUsingAxisNumber() throws WebDriverException, IOException {
		ExtentTest testAxis = extent.createTest("Login using Axis", "Negatif Case : Login using Axis number");
		MobileElement msisdnInput = (MobileElement) driver.findElementByXPath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[2]/android.widget.EditText");
		MobileElement loginButton = (MobileElement) driver.findElementByXPath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[3]/android.widget.Button");
		Assert.assertTrue(msisdnInput.isDisplayed() && loginButton.isDisplayed());
		testAxis.log(Status.INFO, "MSISDN Input & Login Button Displayed", MediaEntityBuilder
				.createScreenCaptureFromBase64String(driver.getScreenshotAs(OutputType.BASE64)).build());

		msisdn = "6283898989968";
		msisdnInput.setValue(msisdn);
		testAxis.log(Status.INFO, "Trying Login using : "+msisdn, MediaEntityBuilder
				.createScreenCaptureFromBase64String(driver.getScreenshotAs(OutputType.BASE64)).build());
		loginButton.click();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

		MobileElement popupText = (MobileElement) driver.findElementByXPath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[2]/android.view.View[3]");
		Assert.assertTrue(popupText.isDisplayed());
		Assert.assertEquals(popupText.getText(), "Anda dapat menggunakan AxisNet untuk mengatur akun Axis Anda.");
		String popupMsg = driver.getScreenshotAs(OutputType.BASE64);
		driver.findElementByXPath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[2]/android.widget.Button[2]")
				.click();
		testAxis.log(Status.PASS, "Login Postpaid passed",
				MediaEntityBuilder.createScreenCaptureFromBase64String(popupMsg).build());
	}

	@Test(enabled = true)
	public void loginusingXL() throws WebDriverException, IOException {
		ExtentTest testXL = extent.createTest("Login using XL number", "Login using XL number");
		MobileElement msisdnInput = (MobileElement) driver.findElementByXPath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[2]/android.widget.EditText");
		MobileElement loginButton = (MobileElement) driver.findElementByXPath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[3]/android.widget.Button");
		Assert.assertTrue(msisdnInput.isDisplayed() && loginButton.isDisplayed());
		testXL.log(Status.INFO, "MSISDN Input & Login Button Displayed", MediaEntityBuilder
				.createScreenCaptureFromBase64String(driver.getScreenshotAs(OutputType.BASE64)).build());
		
		
		
		
		//Test number
//		6285920571209 
//		6285921996234 
//		6285921996232 
//
//		6285921996310 
//		6285959458310
//		6287788060729
//		6285959458344
//		6285920571209 
		
		
		msisdn = "6285920571209";
		//Login Using XL Number
		msisdnInput.setValue(msisdn);
		testXL.log(Status.INFO, "Trying Login using :"+msisdn, MediaEntityBuilder
				.createScreenCaptureFromBase64String(driver.getScreenshotAs(OutputType.BASE64)).build());
		loginButton.click();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

		MobileElement otpText = (MobileElement) driver.findElement(By.xpath("//android.view.View[contains(@text,'Masukkan 6 kode verifikasi yang dikirimkan ke nomor Anda ')]"));
				
		//Assert if otp text exsis
		Assert.assertTrue(otpText.isDisplayed());
		//Assert if send to right number

		try {
			Assert.assertEquals(otpText.getText().split("Anda")[1].trim(), msisdn);
			testXL.log(Status.INFO, "OTP Send to Correct number",MediaEntityBuilder.createScreenCaptureFromBase64String(driver.getScreenshotAs(OutputType.BASE64)).build());
		}catch(AssertionError  e){
			testXL.log(Status.ERROR, "OTP Send to Wrong number",MediaEntityBuilder.createScreenCaptureFromBase64String(driver.getScreenshotAs(OutputType.BASE64)).build());
		}
		
		List<MobileElement> otpField = (List<MobileElement>) driver.findElementsByClassName("android.widget.EditText");
		String otp = "WRGOTP";
		for (int i = 0; i < otpField.size(); i++) {
			otpField.get(i).setValue(String.valueOf(otp.charAt(i)));
		}
		driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[2]/android.view.View[2]/android.widget.Button").click();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		MobileElement errorInd = (MobileElement) driver.findElement(By.xpath("//android.view.View[@text='Kode salah, mohon coba kembali']"));
		Assert.assertTrue(errorInd.isDisplayed());
		Assert.assertEquals(errorInd.getText(), "Kode salah, mohon coba kembali");
		testXL.log(Status.INFO, "Wrong OTP Inputed",MediaEntityBuilder.createScreenCaptureFromBase64String(driver.getScreenshotAs(OutputType.BASE64)).build());
		
//		try {
//			otp = getOTP(msisdn);
//			for (int i = 0; i < otpField.size(); i++) {
//				otpField.get(i).setValue(String.valueOf(otp.charAt(i)));
//			}
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[2]/android.view.View[2]/android.widget.Button").click();
		
		
	}
	
	public static String getOTP() {
//		String sql = "SELECT OTP FROM subs_otp WHERE msisdn = '6285959458310' AND status = 'OPEN' ORDER BY SYS_CREATION_DATE desc";
//		Statement statement = conn.createStatement();
		String otp = null;
//		ResultSet resultSet = statement.executeQuery(sql);
//		while (resultSet.next()) {
//			otp=resultSet.getString("OTP");
//        }
		String devUri = "http://betaweb.xl.co.id/otp/OTPDev?msisdn=";
		String prodUri = "http://betaweb.xl.co.id/otp/OTP?msisdn=";
		otp=getOTP_fromAPI(devUri+"6285920571209");
		System.out.println("OTP : "+otp);
		return otp;
	}
	
	// For testing purpose only
	public static void main(String[] args) {
		getOTP();
	}

}
