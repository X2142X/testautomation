package appiumtests;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Api {
	
	public static String getOTP_fromAPI(String URL) {
        String result = "";
        String credential = Credentials.basic("user", "dsa;lkdsa;lkdsadsa;ldsa;lds;lkds;lk;lkdsa;lkdsa;lk;lk;lkd;lk;lkddsa;lkd;lksa;lkda;lkds");
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(30, TimeUnit.SECONDS); 
        builder.readTimeout(30, TimeUnit.SECONDS); 
        builder.writeTimeout(30, TimeUnit.SECONDS); 
        OkHttpClient client = builder.build();
        Response response = null;
        Request request = new Request.Builder()
                .url(URL)
                .addHeader("Authorization", credential)
                .build();
        try {
            response = client.newCall(request).execute();
            result = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }
}
