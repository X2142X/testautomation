package appiumtests;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class BaseClass extends Api {
	static AppiumDriver driver;
	static AndroidDriver<MobileElement> driver2;
	static ExtentReports extent;
	
	@BeforeSuite
	public static void setupAppium() throws MalformedURLException {

		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability("deviceName", "Samsung S8");
		cap.setCapability("udid", "988a1c323046333534");
		cap.setCapability("platformName", "Android");
		cap.setCapability("platformVersion", "9");
		cap.setCapability("automationName", "UiAutomator2");

		cap.setCapability("app", "C:\\Users\\adrianh\\Downloads\\android-release_26032020.apk");
		cap.setCapability(MobileCapabilityType.NO_RESET, true);
		cap.setCapability("appPackage", "com.apps.MyXL");
		cap.setCapability("appActivity", "com.apps.MyXL.MainActivity");

		URL url = new URL("http://localhost:4723/wd/hub");
		driver = new AppiumDriver<MobileElement>(url, cap);
		driver2 = new AndroidDriver<MobileElement>(url, cap);
		
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		driver.resetApp();

		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("extent.html");
		
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		
//		connectDBMSTRDATADEV();
		
	}
	
	@AfterSuite
	public void uninstallApp() throws InterruptedException, SQLException{
		extent.flush();
//		conn.close();
		driver.removeApp("com.apps.MyXL");
		
	}
}
